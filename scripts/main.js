(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
$(document).ready(function() {
    // initialise foundation components
    // $(document).foundation();
    // $('.js-header-carousel').slick();

    var slidesOptions = {
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.js-header-carousel-nav'
    },
    navOptions = {
      slidesToScroll: 1,
      asNavFor: '.js-header-carousel-slides',
      fade: true,
      prevArrow: '<button type="button" class="slick-prev header-carousel__prev"><span class="icon icon_arrow icon_left-arrow"> <svg title="search"> <use xlink:href="/icons/icon-sprite.svg#icon-left-arrow"></use> </svg></span></button>',
      nextArrow: '<button type="button" class="slick-next header-carousel__next"><span class="icon icon_arrow icon_right-arrow"> <svg title="search"> <use xlink:href="/icons/icon-sprite.svg#icon-right-arrow"></use> </svg></span></button>'
    };

     $('.js-header-carousel-slides').slick({
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.js-header-carousel-nav'
    });

    $('.js-header-carousel-nav').slick({
      slidesToScroll: 1,
      asNavFor: '.js-header-carousel-slides',
      fade: true,
      prevArrow: '<button type="button" class="slick-prev header-carousel__prev"><span class="icon icon_arrow icon_left-arrow"> <svg title="search"> <use xlink:href="/icons/icon-sprite.svg#icon-left-arrow"></use> </svg></span></button>',
      nextArrow: '<button type="button" class="slick-next header-carousel__next"><span class="icon icon_arrow icon_right-arrow"> <svg title="search"> <use xlink:href="/icons/icon-sprite.svg#icon-right-arrow"></use> </svg></span></button>'
    });

    $('.js-content-carousel-slides').slick({
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.js-content-carousel-nav'
    });

    $('.js-content-carousel-nav').slick({
      slidesToScroll: 1,
      asNavFor: '.js-content-carousel-slides',
      fade: true,
      prevArrow: '<button type="button" class="slick-prev content-carousel__prev"><span class="icon icon_arrow icon_left-arrow"> <svg title="search"> <use xlink:href="/icons/icon-sprite.svg#icon-left-arrow"></use> </svg></span></button>',
      nextArrow: '<button type="button" class="slick-next content-carousel__next"><span class="icon icon_arrow icon_right-arrow"> <svg title="search"> <use xlink:href="/icons/icon-sprite.svg#icon-right-arrow"></use> </svg></span></button>'
    });
});

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJhcHAvc2NyaXB0cy9tYWluLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcbiAgICAvLyBpbml0aWFsaXNlIGZvdW5kYXRpb24gY29tcG9uZW50c1xuICAgIC8vICQoZG9jdW1lbnQpLmZvdW5kYXRpb24oKTtcbiAgICAvLyAkKCcuanMtaGVhZGVyLWNhcm91c2VsJykuc2xpY2soKTtcblxuICAgIHZhciBzbGlkZXNPcHRpb25zID0ge1xuICAgICAgICBzbGlkZXNUb1Njcm9sbDogMSxcbiAgICAgICAgYXJyb3dzOiBmYWxzZSxcbiAgICAgICAgZmFkZTogdHJ1ZSxcbiAgICAgICAgYXNOYXZGb3I6ICcuanMtaGVhZGVyLWNhcm91c2VsLW5hdidcbiAgICB9LFxuICAgIG5hdk9wdGlvbnMgPSB7XG4gICAgICBzbGlkZXNUb1Njcm9sbDogMSxcbiAgICAgIGFzTmF2Rm9yOiAnLmpzLWhlYWRlci1jYXJvdXNlbC1zbGlkZXMnLFxuICAgICAgZmFkZTogdHJ1ZSxcbiAgICAgIHByZXZBcnJvdzogJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwic2xpY2stcHJldiBoZWFkZXItY2Fyb3VzZWxfX3ByZXZcIj48c3BhbiBjbGFzcz1cImljb24gaWNvbl9hcnJvdyBpY29uX2xlZnQtYXJyb3dcIj4gPHN2ZyB0aXRsZT1cInNlYXJjaFwiPiA8dXNlIHhsaW5rOmhyZWY9XCIvaWNvbnMvaWNvbi1zcHJpdGUuc3ZnI2ljb24tbGVmdC1hcnJvd1wiPjwvdXNlPiA8L3N2Zz48L3NwYW4+PC9idXR0b24+JyxcbiAgICAgIG5leHRBcnJvdzogJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwic2xpY2stbmV4dCBoZWFkZXItY2Fyb3VzZWxfX25leHRcIj48c3BhbiBjbGFzcz1cImljb24gaWNvbl9hcnJvdyBpY29uX3JpZ2h0LWFycm93XCI+IDxzdmcgdGl0bGU9XCJzZWFyY2hcIj4gPHVzZSB4bGluazpocmVmPVwiL2ljb25zL2ljb24tc3ByaXRlLnN2ZyNpY29uLXJpZ2h0LWFycm93XCI+PC91c2U+IDwvc3ZnPjwvc3Bhbj48L2J1dHRvbj4nXG4gICAgfTtcblxuICAgICAkKCcuanMtaGVhZGVyLWNhcm91c2VsLXNsaWRlcycpLnNsaWNrKHtcbiAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxuICAgICAgYXJyb3dzOiBmYWxzZSxcbiAgICAgIGZhZGU6IHRydWUsXG4gICAgICBhc05hdkZvcjogJy5qcy1oZWFkZXItY2Fyb3VzZWwtbmF2J1xuICAgIH0pO1xuXG4gICAgJCgnLmpzLWhlYWRlci1jYXJvdXNlbC1uYXYnKS5zbGljayh7XG4gICAgICBzbGlkZXNUb1Njcm9sbDogMSxcbiAgICAgIGFzTmF2Rm9yOiAnLmpzLWhlYWRlci1jYXJvdXNlbC1zbGlkZXMnLFxuICAgICAgZmFkZTogdHJ1ZSxcbiAgICAgIHByZXZBcnJvdzogJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwic2xpY2stcHJldiBoZWFkZXItY2Fyb3VzZWxfX3ByZXZcIj48c3BhbiBjbGFzcz1cImljb24gaWNvbl9hcnJvdyBpY29uX2xlZnQtYXJyb3dcIj4gPHN2ZyB0aXRsZT1cInNlYXJjaFwiPiA8dXNlIHhsaW5rOmhyZWY9XCIvaWNvbnMvaWNvbi1zcHJpdGUuc3ZnI2ljb24tbGVmdC1hcnJvd1wiPjwvdXNlPiA8L3N2Zz48L3NwYW4+PC9idXR0b24+JyxcbiAgICAgIG5leHRBcnJvdzogJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwic2xpY2stbmV4dCBoZWFkZXItY2Fyb3VzZWxfX25leHRcIj48c3BhbiBjbGFzcz1cImljb24gaWNvbl9hcnJvdyBpY29uX3JpZ2h0LWFycm93XCI+IDxzdmcgdGl0bGU9XCJzZWFyY2hcIj4gPHVzZSB4bGluazpocmVmPVwiL2ljb25zL2ljb24tc3ByaXRlLnN2ZyNpY29uLXJpZ2h0LWFycm93XCI+PC91c2U+IDwvc3ZnPjwvc3Bhbj48L2J1dHRvbj4nXG4gICAgfSk7XG5cbiAgICAkKCcuanMtY29udGVudC1jYXJvdXNlbC1zbGlkZXMnKS5zbGljayh7XG4gICAgICBzbGlkZXNUb1Njcm9sbDogMSxcbiAgICAgIGFycm93czogZmFsc2UsXG4gICAgICBmYWRlOiB0cnVlLFxuICAgICAgYXNOYXZGb3I6ICcuanMtY29udGVudC1jYXJvdXNlbC1uYXYnXG4gICAgfSk7XG5cbiAgICAkKCcuanMtY29udGVudC1jYXJvdXNlbC1uYXYnKS5zbGljayh7XG4gICAgICBzbGlkZXNUb1Njcm9sbDogMSxcbiAgICAgIGFzTmF2Rm9yOiAnLmpzLWNvbnRlbnQtY2Fyb3VzZWwtc2xpZGVzJyxcbiAgICAgIGZhZGU6IHRydWUsXG4gICAgICBwcmV2QXJyb3c6ICc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cInNsaWNrLXByZXYgY29udGVudC1jYXJvdXNlbF9fcHJldlwiPjxzcGFuIGNsYXNzPVwiaWNvbiBpY29uX2Fycm93IGljb25fbGVmdC1hcnJvd1wiPiA8c3ZnIHRpdGxlPVwic2VhcmNoXCI+IDx1c2UgeGxpbms6aHJlZj1cIi9pY29ucy9pY29uLXNwcml0ZS5zdmcjaWNvbi1sZWZ0LWFycm93XCI+PC91c2U+IDwvc3ZnPjwvc3Bhbj48L2J1dHRvbj4nLFxuICAgICAgbmV4dEFycm93OiAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJzbGljay1uZXh0IGNvbnRlbnQtY2Fyb3VzZWxfX25leHRcIj48c3BhbiBjbGFzcz1cImljb24gaWNvbl9hcnJvdyBpY29uX3JpZ2h0LWFycm93XCI+IDxzdmcgdGl0bGU9XCJzZWFyY2hcIj4gPHVzZSB4bGluazpocmVmPVwiL2ljb25zL2ljb24tc3ByaXRlLnN2ZyNpY29uLXJpZ2h0LWFycm93XCI+PC91c2U+IDwvc3ZnPjwvc3Bhbj48L2J1dHRvbj4nXG4gICAgfSk7XG59KTtcbiJdfQ==
